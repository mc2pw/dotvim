"
" dotphiles : https://github.com/dotphiles/dotphiles
"
" Essential vim plugins!
"
" Authors:
"   Ben O'Hara <bohara@gmail.com>
"

" Vundle itself
Plugin 'file://'.$VIM_PLUGIN_DIR.'/VundleVim/Vundle.vim.git'

" General
if count(g:vundles, 'general')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/scrooloose/nerdtree.git'
  ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/chriskempson/base16-vim.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/tomasr/molokai.git'
  ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/YankRing.vim.git'
  ""let g:yankring_history_dir = $HOME.'/.vim/'
  ""let g:yankring_history_file = '.yankring_history'
  ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/tpope/vim-repeat.git'
  ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/AutoClose.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/kien/ctrlp.vim.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/jistr/vim-nerdtree-tabs.git'
  ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/sudo.vim.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/bronson/vim-trailing-whitespace.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/mbbill/undotree.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/mhinz/vim-signify.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/Raimondi/delimitMate.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/vim-airline/vim-airline.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/vim-airline/vim-airline-themes.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/rodjek/vim-puppet.git'
endif

if count(g:vundles, 'git')
  if executable('git')
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/tpope/vim-fugitive.git'
  endif
endif

if count(g:vundles, 'hg')
  if executable('hg')
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/k-takata/hg-vim.git'
  endif
endif
" General Programming
if count(g:vundles, 'programming')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/godlygeek/tabular.git'
  if executable('ack')
    ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/mileszs/ack.vim.git'
  endif
  if v:version > 700
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/scrooloose/syntastic.git'
    if executable('ctags')
      ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/majutsushi/tagbar.git'
    endif
endif
endif

if count(g:vundles, 'completion')
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/Shougo/neocomplcache.git'
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/Shougo/neosnippet.git'
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/Shougo/neosnippet-snippets.git'
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/scrooloose/snipmate-snippets.git'
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/honza/vim-snippets.git'
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/vim-scripts/Indent-Guides.git'
endif

" PHP
if count(g:vundles, 'php')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/spf13/PIV.git'
endif

" Python
if count(g:vundles, 'python')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/hynek/vim-python-pep8-indent.git'
  let g:syntastic_python_flake8_args='--ignore=E501'
endif

" Javascript
if count(g:vundles, 'javascript')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/leshill/vim-json.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/groenewege/vim-less.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/taxilian/vim-web-indent.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/pangloss/vim-javascript.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/mxw/vim-jsx.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/leafgarland/typescript-vim.git'
endif

" HTML
if count(g:vundles, 'html')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/vim-scripts/HTML-AutoCloseTag.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/ChrisYip/Better-CSS-Syntax-for-Vim.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/juvenn/mustache.vim.git'
endif

" Ruby
if count(g:vundles, 'ruby')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/tpope/vim-rails.git'
endif

" Go
if count(g:vundles, 'go')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/fatih/vim-go.git'
endif

" Misc
if count(g:vundles, 'misc')
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/tpope/vim-markdown.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/spf13/vim-preview.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/tpope/vim-cucumber.git'
  ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/markcornick/vim-vagrant.git'
  ""Plugin 'file://'.$VIM_PLUGIN_DIR.'/Puppet-Syntax-Highlighting.git'
  Plugin 'file://'.$VIM_PLUGIN_DIR.'/ekalinin/Dockerfile.vim.git'
  if v:version > 701
    Plugin 'file://'.$VIM_PLUGIN_DIR.'/zhaocai/GoldenView.Vim.git'
  endif
  let g:goldenview__enable_default_mapping=0
endif

